import React from 'react';
import logo from './logo.svg';
import VideoStream from "./VideoStream"
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <VideoStream wsUrl="ws://192.168.1.11:8000/video_feed"/>
      </header>
    </div>
  );
}

export default App;
